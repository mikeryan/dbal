<?php
declare(strict_types=1);

namespace Soong\DBAL;

use Doctrine\DBAL\DBALException;
use Soong\Data\DataRecordInterface;
use Soong\Data\Property;
use Soong\Loader\LoaderBase;

/**
 * Loader for DBAL SQL tables.
 *
 * @package Soong\DBAL
 */
class Loader extends LoaderBase
{

    use DBALTrait;

    /**
     * {@inheritdoc}
     */
    public function load(DataRecordInterface $data) : void
    {
        try {
            $this->connection()->insert(
                $this->configuration['table'],
                $data->toArray()
            );
            $id = $this->connection()->lastInsertId();
            if ($id) {
                $keyKeys = array_keys($this->getKeyProperties());
                $data->setProperty(reset($keyKeys), new Property($id));
            }
        } catch (DBALException $e) {
            print $e->getMessage();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProperties(): array
    {
        return $this->configuration['properties'];
    }

    /**
     * {@inheritdoc}
     */
    public function getKeyProperties(): array
    {
        return $this->configuration['key_properties'];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(array $key) : void
    {
        $queryBuilder = $this->connection()->createQueryBuilder();
        $queryBuilder->delete($this->configuration['table']);
        $counter = 0;
        foreach ($key as $name => $value) {
            $queryBuilder->andWhere("$name = ?");
            $queryBuilder->setParameter($counter, $value);
            $counter++;
        }
        $queryBuilder->execute();
    }
}
