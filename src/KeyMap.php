<?php
declare(strict_types=1);

namespace Soong\DBAL;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Schema\Table;
use Soong\KeyMap\KeyMapBase;

/**
 * Implementation of key maps using DBAL for storage.
 *
 * @package Soong\DBAL
 */
class KeyMap extends KeyMapBase
{

    use DBALTrait;

    /**
     * Prefixes for database column names.
     */
    const EXTRACTED_KEY_PREFIX = 'extracted_key_';
    const LOADED_KEY_PREFIX = 'loaded_key_';

    /**
     * The database table name for the key map.
     *
     * @var string
     */
    protected $tableName;

    /**
     * Save the table name.
     *
     * @param array $configuration
     *   Configuration values keyed by configuration name.
     */
    public function __construct(array $configuration)
    {
        parent::__construct($configuration);
        $this->tableName = $this->configuration['table'];
    }

    /**
     * {@inheritdoc}
     */
    public function saveKeyMap(array $extractedKey, array $loadedKey) : void
    {
        if (!$this->tableExists()) {
            $this->createTable();
        }
        $extractedKey = array_values($extractedKey);
        $loadedKey = array_values($loadedKey);
        $queryBuilder = $this->connection()->createQueryBuilder();
        $queryBuilder->insert($this->tableName);
        $parameterNumber = 0;
        $queryBuilder->setValue('hash', '?');
        $queryBuilder->setParameter($parameterNumber++, $this->hashKeys($extractedKey));
        $counter = 0;
        foreach ($this->extractedKeyColumns() as $columnName) {
            $queryBuilder->setValue($columnName, '?');
            $queryBuilder->setParameter($parameterNumber++, $extractedKey[$counter]);
            $counter++;
        }
        $counter = 0;
        foreach ($this->loadedKeyColumns() as $columnName) {
            $queryBuilder->setValue($columnName, '?');
            $queryBuilder->setParameter($parameterNumber++, $loadedKey[$counter]);
            $counter++;
        }
        $queryBuilder->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function lookupLoadedKey(array $extractedKey) : array
    {
        if (!$this->tableExists()) {
            return [];
        }
        $queryBuilder = $this->connection()->createQueryBuilder();
        foreach ($this->loadedKeyColumns() as $keyName => $columnName) {
            $queryBuilder->addSelect("$columnName as $keyName");
        }
        $queryBuilder
          ->from($this->tableName)
          ->where('hash = ?')
          ->setParameter(0, $this->hashKeys($extractedKey));
        $result = $queryBuilder->execute()->fetch(FetchMode::ASSOCIATIVE);
        if (!$result) {
            return [];
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function lookupExtractedKeys(array $loadedKey) : array
    {
        if (!$this->tableExists()) {
            return [];
        }
        $queryBuilder = $this->connection()->createQueryBuilder();
        foreach ($this->extractedKeyColumns() as $columnName) {
            $queryBuilder->addSelect($columnName);
        }
        $queryBuilder->from($this->tableName);
        $loadedKeys = array_values($loadedKey);
        $counter = 0;
        foreach ($this->loadedKeyColumns() as $columnName) {
            $queryBuilder->andWhere("$columnName = ?");
            $queryBuilder->setParameter($counter, $loadedKeys[$counter]);
            $counter++;
        }
        $result = $queryBuilder->execute()->fetchAll(FetchMode::ASSOCIATIVE);
        if (!$result) {
            return [];
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(array $extractedKey) : void
    {
        if (!$this->tableExists()) {
            return;
        }
        $queryBuilder = $this->connection()->createQueryBuilder();
        $queryBuilder->delete($this->tableName)
            ->where('hash = ?')
            ->setParameter(0, $this->hashKeys($extractedKey));
        $queryBuilder->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
        if (!$this->tableExists()) {
            return 0;
        }
        try {
            $count = $this->connection()
                ->executeQuery("SELECT COUNT(*) FROM {$this->tableName}")
                ->fetchColumn();
        } catch (DBALException $e) {
            print $e->getMessage();
        }
        return (int)$count;
    }

    /**
     * Create the key map table if it doesn't already exist.
     */
    protected function createTable()  : void
    {
        if (!$this->tableExists()) {
            $schema = $this->connection()->getSchemaManager();
            try {
                $map = new Table($this->tableName);
                $sampleHash = $this->hashKeys(['foo', 'bar']);
                $map->addColumn(
                    'hash',
                    'string',
                    ['length' => strlen($sampleHash), 'not null' => true]
                );
                $map->setPrimaryKey(['hash']);
                $counter = 1;
                $indexColumns = [];
                foreach ($this->configuration[self::CONFIGURATION_EXTRACTOR_KEYS] as $definition) {
                    $type = $definition['type'];
                    $options = $definition['options'] ?? [];
                    $columnName = self::EXTRACTED_KEY_PREFIX . $counter++;
                    $map->addColumn($columnName, $type, $options);
                    $indexColumns[] = $columnName;
                }
                $map->addIndex($indexColumns);
                $counter = 1;
                foreach ($this->configuration[self::CONFIGURATION_LOADER_KEYS] as $definition) {
                    $type = $definition['type'];
                    $options = $definition['options'] ?? [];
                    $columnName = self::LOADED_KEY_PREFIX . $counter++;
                    $map->addColumn($columnName, $type, $options);
                }
                $schema->createTable($map);
            } catch (DBALException $e) {
                print $e->getMessage();
            }
        }
    }

    /**
     * See if the database table exists.
     *
     * @return bool
     *   TRUE if the table exists, FALSE otherwise.
     */
    protected function tableExists() : bool
    {
        $schema = $this->connection()->getSchemaManager();
        return $schema->tablesExist([$this->tableName]);
    }

    /**
     * Construct and return the names of the extracted key columns.
     *
     * @return array
     *   List of extracted key column names, keyed by the extracted key name.
     */
    protected function extractedKeyColumns() : array
    {
        $counter = 1;
        foreach (array_keys($this->configuration[self::CONFIGURATION_EXTRACTOR_KEYS]) as $keyName) {
            $columnName = self::EXTRACTED_KEY_PREFIX . $counter++;
            $result[$keyName] = $columnName;
        }
        return $result;
    }

    /**
     * Construct and return the names of the loaded key columns.
     *
     * @return array
     *   List of loaded key column names, keyed by the loaded key name.
     */
    protected function loadedKeyColumns() : array
    {
        $counter = 1;
        foreach (array_keys($this->configuration[self::CONFIGURATION_LOADER_KEYS]) as $keyName) {
            $columnName = self::LOADED_KEY_PREFIX . $counter++;
            $result[$keyName] = $columnName;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function iterate() : iterable
    {
        if ($this->tableExists()) {
            $queryBuilder = $this->connection()->createQueryBuilder();
            $queryBuilder->from($this->tableName);
            foreach ($this->extractedKeyColumns() as $keyName => $columnName) {
                $queryBuilder->addSelect("$columnName AS $keyName");
            }
            foreach ($queryBuilder->execute() as $row) {
                yield $row;
            }
        }
    }
}
