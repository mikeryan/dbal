<?php
declare(strict_types=1);

namespace Soong\DBAL;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;

/**
 * Common implementation for Soong's DBAL implementations.
 *
 * @package Soong\DBAL
 */
trait DBALTrait
{

    /**
     * The DBAL Connection.
     *
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * Make (if necessary) and return the connection to the specified database.
     *
     * @return \Doctrine\DBAL\Connection|null
     */
    protected function connection() : ?Connection
    {
        if (empty($this->connection)) {
            try {
                $this->connection = DriverManager::getConnection($this->configuration['connection']);
            } catch (DBALException $e) {
                print $e->getMessage();
                return null;
            }
        }
        return $this->connection;
    }
}
