<?php
declare(strict_types=1);

namespace Soong\DBAL;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Soong\Extractor\CountableExtractorBase;

/**
 * Extractor for DBAL SQL queries.
 *
 * @package Soong\DBAL
 */
class Extractor extends CountableExtractorBase
{

    use DBALTrait;

    /**
     * {@inheritdoc}
     */
    public function extractAll() : iterable
    {
        try {
            // @todo: don't accept raw SQL from configuration
            /** @var \Doctrine\DBAL\Driver\Statement $statement */
            $statement = $this->connection()->executeQuery($this->configuration['query']);
            while ($row = $statement->fetch(FetchMode::ASSOCIATIVE)) {
                $dataRecordClass = $this->configuration['data_record_class'];
                /** @var \Soong\Data\DataRecordInterface $dataObject */
                $dataObject = new $dataRecordClass();
                $dataObject->fromArray($row);
                yield $dataObject;
            }
        } catch (DBALException $e) {
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProperties(): array
    {
        // @todo: Identify properties from the query.
        return parent::getProperties();
    }

    // @todo implement count() to use SQL COUNT().
}
